source $HOME/etc/profile

setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments
setopt share_history # Share history between all zsh sessions
setopt hist_ignore_all_dups # DUPS = duplicates

HISTSIZE=10000000
SAVEHIST=10000000

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Vi mode
bindkey -v
export KEYTIMEOUT=1

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Get a good-looking cursor
function zle-keymap-select() {
	case $KEYMAP in
		vicmd) echo -ne '\e[1 q' ;;
		viins|main) echo -ne '\e[5 q' ;;
	esac
}
zle -N zle-keymap-select
_fix_cursor() {
	echo -ne '\e[5 q'
}
precmd_functions+=(_fix_cursor)

# Colors + PS1
autoload -U colors && colors
PS1="%B%{$fg[cyan]%}[%n@%M %{$fg[white]%}%~%{$fg[cyan]%}]$%{$reset_color%}%b "

## ALIASES ##
# Functional aliases
alias startx="startx $XINITRC"

# Nail-polish
alias ls="ls --color=auto -F"
alias grep="grep --color=auto"

# Setting up git repositories that control $HOME
gitRepo() {
	echo "git --git-dir=$1 --work-tree=$2"
}
alias dot="$(gitRepo $XDG_SRC_HOME/dots $HOME)"

## LESS COLORS (pretty colors hmm)
#export LESS=-R
#export LESSOPEN="| src-hilite-lesspipe.sh %s"
#export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
#export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
#export LESS_TERMCAP_me="$(printf '%b' '[0m')"
##export LESS_TERMCAP_se="$(printf '%b' '[0m')"
#export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
#export LESS_TERMCAP_ue="$(printf '%b' '[0m')"

## YES INSULT ME PLEASE ##
if [ -f /etc/bash.command-not-found ]; then
	. /etc/bash.command-not-found
fi

# Loads syntax highlighting
. /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh

## DELETING DUMB FUCKING ZSH_HISTORY, THAT IS CREATED EVEN THOUGH I SPECIFICALLY ASKED IT TO NOT ##
test -f $HOME/.zsh_history && rm $HOME/.zsh_history
