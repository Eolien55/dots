cols = {
    'bg': '#2E3440',
    'fg': '#ECEFF4',
    'hard_bg': '#2E3440',
    'soft_bg': '#3B4252',
    'soft_soft_bg': '#434C5E',
    'black': '#3B4252',
    'darkgrey': '#434C5E',
    'darkred': '#BF616A',
    'red': '#BF616A',
    'darkgreen': '#A3BE8C',
    'green': '#A3BE8C',
    'darkyellow': '#EBCB8B',
    'yellow': '#EBCB8B',
    'darkblue': '#5E81AC',
    'blue': '#81A1C1',
    'darkmagenta': '#B48EAD',
    'magenta': '#B48EAD',
    'darkcyan': '#8FBCBB',
    'cyan': '#88C0D0',
    'lightgrey': '#D8DEE9',
    'white': '#ECEFF4'
}

## Background color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.bg = cols['bg']

## Bottom border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.border.bottom = cols['bg']

## Top border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.border.top = cols['bg']

## Foreground color of completion widget category headers.
## Type: QtColor
c.colors.completion.category.fg = cols['fg']

## Background color of the completion widget for even rows.
## Type: QssColor
c.colors.completion.even.bg = cols['soft_soft_bg']

## Background color of the completion widget for odd rows.
## Type: QssColor
c.colors.completion.odd.bg = cols['soft_soft_bg']

## Text color of the completion widget.
## Type: QtColor
c.colors.completion.fg = cols['lightgrey']

## Background color of the selected completion item.
## Type: QssColor
c.colors.completion.item.selected.bg = cols['soft_soft_bg']

## Bottom border color of the selected completion item.
## Type: QssColor
c.colors.completion.item.selected.border.bottom = cols['soft_soft_bg']

## Top border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.item.selected.border.top = cols['soft_soft_bg']

## Foreground color of the selected completion item.
## Type: QtColor
c.colors.completion.item.selected.fg = cols['white']

## Foreground color of the matched text in the completion.
## Type: QssColor
c.colors.completion.match.fg = cols['yellow']

## Color of the scrollbar in completion view
## Type: QssColor
c.colors.completion.scrollbar.bg = cols['soft_soft_bg']

## Color of the scrollbar handle in completion view.
## Type: QssColor
c.colors.completion.scrollbar.fg = cols['fg']

## Background color for the download bar.
## Type: QssColor
c.colors.downloads.bar.bg = cols['bg']

## Background color for downloads with errors.
## Type: QtColor
c.colors.downloads.error.bg = cols['darkred']

## Foreground color for downloads with errors.
## Type: QtColor
c.colors.downloads.error.fg = cols['fg']

## Color gradient stop for download backgrounds.
## Type: QtColor
c.colors.downloads.stop.bg = cols['magenta']

## Color gradient interpolation system for download backgrounds.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
c.colors.downloads.system.bg = 'none'

## Background color for hints. Note that you can use a `rgba(...)` value
## for transparency.
## Type: QssColor
c.colors.hints.bg = cols['yellow']

## Font color for hints.
## Type: QssColor
c.colors.hints.fg = cols['bg']

## Font color for the matched part of hints.
## Type: QssColor
c.colors.hints.match.fg = cols['darkcyan']

## Background color of the keyhint widget.
## Type: QssColor
c.colors.keyhint.bg = cols['soft_bg']

## Text color for the keyhint widget.
## Type: QssColor
c.colors.keyhint.fg = cols['fg']

## Highlight color for keys to complete the current keychain.
## Type: QssColor
c.colors.keyhint.suffix.fg = cols['yellow']

## Background color of an error message.
## Type: QssColor
c.colors.messages.error.bg = cols['darkred']

## Border color of an error message.
## Type: QssColor
c.colors.messages.error.border = cols['darkred']

## Foreground color of an error message.
## Type: QssColor
c.colors.messages.error.fg = cols['fg']

## Background color of an info message.
## Type: QssColor
c.colors.messages.info.bg = cols['cyan']

## Border color of an info message.
## Type: QssColor
c.colors.messages.info.border = cols['cyan']

## Foreground color an info message.
## Type: QssColor
c.colors.messages.info.fg = cols['fg']

## Background color of a warning message.
## Type: QssColor
c.colors.messages.warning.bg = cols['red']

## Border color of a warning message.
## Type: QssColor
c.colors.messages.warning.border = cols['red']

## Foreground color a warning message.
## Type: QssColor
c.colors.messages.warning.fg = cols['fg']

## Background color for prompts.
## Type: QssColor
c.colors.prompts.bg = cols['soft_bg']

# ## Border used around UI elements in prompts.
# ## Type: String
c.colors.prompts.border = '1px solid ' + cols['bg']

## Foreground color for prompts.
## Type: QssColor
c.colors.prompts.fg = cols['fg']

## Background color for the selected item in filename prompts.
## Type: QssColor
c.colors.prompts.selected.bg = cols['soft_bg']

## Background color of the statusbar in caret mode.
## Type: QssColor
c.colors.statusbar.caret.bg = cols['magenta']

## Foreground color of the statusbar in caret mode.
## Type: QssColor
c.colors.statusbar.caret.fg = cols['fg']

## Background color of the statusbar in caret mode with a selection.
## Type: QssColor
c.colors.statusbar.caret.selection.bg = cols['magenta']

## Foreground color of the statusbar in caret mode with a selection.
## Type: QssColor
c.colors.statusbar.caret.selection.fg = cols['fg']

## Background color of the statusbar in command mode.
## Type: QssColor
c.colors.statusbar.command.bg = cols['soft_bg']

## Foreground color of the statusbar in command mode.
## Type: QssColor
c.colors.statusbar.command.fg = cols['fg']

## Background color of the statusbar in private browsing + command mode.
## Type: QssColor
c.colors.statusbar.command.private.bg = cols['soft_bg']

## Foreground color of the statusbar in private browsing + command mode.
## Type: QssColor
c.colors.statusbar.command.private.fg = cols['fg']

## Background color of the statusbar in insert mode.
## Type: QssColor
c.colors.statusbar.insert.bg = cols['green']

## Foreground color of the statusbar in insert mode.
## Type: QssColor
c.colors.statusbar.insert.fg = cols['soft_bg']

## Background color of the statusbar.
## Type: QssColor
c.colors.statusbar.normal.bg = cols['bg']

## Foreground color of the statusbar.
## Type: QssColor
c.colors.statusbar.normal.fg = cols['fg']

## Background color of the statusbar in passthrough mode.
## Type: QssColor
c.colors.statusbar.passthrough.bg = cols['darkcyan']

## Foreground color of the statusbar in passthrough mode.
## Type: QssColor
c.colors.statusbar.passthrough.fg = cols['fg']

## Background color of the statusbar in private browsing mode.
## Type: QssColor
c.colors.statusbar.private.bg = cols['soft_bg']

## Foreground color of the statusbar in private browsing mode.
## Type: QssColor
c.colors.statusbar.private.fg = cols['fg']

## Background color of the progress bar.
## Type: QssColor
c.colors.statusbar.progress.bg = cols['fg']

## Foreground color of the URL in the statusbar on error.
## Type: QssColor
c.colors.statusbar.url.error.fg = cols['darkred']

## Default foreground color of the URL in the statusbar.
## Type: QssColor
c.colors.statusbar.url.fg = cols['fg']

## Foreground color of the URL in the statusbar for hovedarkred links.
## Type: QssColor
c.colors.statusbar.url.hover.fg = cols['cyan']

## Foreground color of the URL in the statusbar on successful load
## (http).
## Type: QssColor
c.colors.statusbar.url.success.http.fg = cols['fg']

## Foreground color of the URL in the statusbar on successful load
## (https).
## Type: QssColor
c.colors.statusbar.url.success.https.fg = cols['green']

## Foreground color of the URL in the statusbar when there's a warning.
## Type: QssColor
c.colors.statusbar.url.warn.fg = cols['red']

## Background color of the tab bar.
## Type: QtColor
c.colors.tabs.bar.bg = cols['soft_soft_bg']

## Background color of unselected even tabs.
## Type: QtColor
c.colors.tabs.even.bg = cols['soft_soft_bg']

## Foreground color of unselected even tabs.
## Type: QtColor
c.colors.tabs.even.fg = cols['fg']

## Color for the tab indicator on errors.
## Type: QtColor
c.colors.tabs.indicator.error = cols['darkred']

## Color gradient start for the tab indicator.
## Type: QtColor
# c.colors.tabs.indicator.start = cols['violet']

## Color gradient end for the tab indicator.
## Type: QtColor
# c.colors.tabs.indicator.stop = cols['orange']

## Color gradient interpolation system for the tab indicator.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
c.colors.tabs.indicator.system = 'none'

## Background color of unselected odd tabs.
## Type: QtColor
c.colors.tabs.odd.bg = cols['soft_soft_bg']

## Foreground color of unselected odd tabs.
## Type: QtColor
c.colors.tabs.odd.fg = cols['fg']

# ## Background color of selected even tabs.
# ## Type: QtColor
c.colors.tabs.selected.even.bg = cols['hard_bg']

# ## Foreground color of selected even tabs.
# ## Type: QtColor
c.colors.tabs.selected.even.fg = cols['fg']

# ## Background color of selected odd tabs.
# ## Type: QtColor
c.colors.tabs.selected.odd.bg = cols['hard_bg']

# ## Foreground color of selected odd tabs.
# ## Type: QtColor
c.colors.tabs.selected.odd.fg = cols['fg']

## Background color for webpages if unset (or empty to use the theme's
## color)
## Type: QtColor
# c.colors.webpage.bg = cols['bg']
