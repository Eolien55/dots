// ==UserScript==
// @name	Dark Reader
// @icon	https://darkreader.org/images/darkreader-icon-256x256.png
// @namespace	DarkReader
// @description	Smart dark mode
// @version	4.9.52
// @author	https://github.com/darkreader/darkreader#contributors
// @homepageURL	https://darkreader.org
// @run-at	document-end
// @grant	none
// @include	http*
// @exclude	https://discord.com/*
// @exclude	https://codeberg.org/*
// @exclude	https://parol.martinrue.com/*
// @require	https://cdn.jsdelivr.net/npm/darkreader/darkreader.min.js
// @noframes
// ==/UserScript==

DarkReader.auto({
	brightness: 100,
	contraset: 100,
	sepia: 0
});
