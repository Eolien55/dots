local o = vim.opt

o.number = true
o.relativenumber = true
o.cursorline = true
o.cursorlineopt = "number"

vim.cmd [[highlight LineNr ctermfg=7]]
vim.cmd [[highlight CursorLineNr ctermfg=7 cterm=NONE]]
