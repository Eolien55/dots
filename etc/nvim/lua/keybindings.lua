local map = require("utils").map

local M = {}

M.general = {
  i = {
    -- Vim movement in insert mode
    ["<C-h>"] = { "<Left>", "move left" },
    ["<C-l>"] = { "<Right>", "move right" },
    ["<C-j>"] = { "<Down>", "move right" },
    ["<C-k>"] = { "<Up>", "move right" },
  }
}

return M
