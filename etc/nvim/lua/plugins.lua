local packer = require('packer')

packer.startup(function(use)
	-- Packer manages itself !
	use 'wbthomason/packer.nvim'

	-- Improved syntax highlighting
	use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate', config = require('nvim-treesitter.configs').setup { highlight = { enable = true }}}

	-- Comment utilities
	use { 
		'numToStr/Comment.nvim',
		config = function()
		    require('Comment').setup()
	        end
	}

	use {
		"nvim-lualine/lualine.nvim",
		requires = { "kyazdani42/nvim-web-devicons", opt = true },
		config = function() require('lualine').setup { options = { theme = require('plugins.lualine') } } end
	}

	use {
		'kylechui/nvim-surround',
		 config = function()
		     require("nvim-surround").setup({
		         -- Configuration here, or leave empty to use defaults
		     })
		 end
	 }
end)
