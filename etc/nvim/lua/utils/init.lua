local M = {}

function M.map(mode, lhs, rhs, opts)
    local options = { noremap = true }
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end


function M.apply_bindings(bindings)
    for name, val in pairs(bindings) do
        for mode, bindings in pairs(val) do
            for binding, meaning in pairs(bindings) do
	        M.map(mode, binding, meaning[1], meaning[3])
	    end
        end
    end
end


return M
