#!/bin/sh

# To put in the directory that you want to organise with this tool


# The format for files is the following : <code>...:<filename>

# Codes are separated by ":". Each piece separated by a ':' thus corresponds to
# a filesystem "scale" (directory, subdirectory, file, etc.).

# If a code begins by ",", it will be prepended the code that comes just
# before, so l:,1 becomes l:l1

# Codes are replaced by their equivalent (see CODES variable)

# ':' are replaced by '/' in file name

CODES="l:lycée; l1:première
l1fr:français;	l1fr1:personnages en marge et plaisir du romanesque
l1gr:grec;	l1gr1:la parole : pouvoirs et dérives
l1en:english"
CODES="$(echo "$CODES" | sed '/^$/d; s/;\s*/\n/g')"

filter() {
	cut -d' ' -f2- | sed_op
}

sed_op() {
	SEDCOMMANDS="$(echo "$CODES" | sed 's/^\([^:]*\):\(.*\)$/s|\/\1\/|\/\2\/|g/')"
	tmpfile="$(mktemp)"
	trap 'rm "$tmpfile"' 0 1 15
	sed 's|^|/|; s|:|/|g' > "$tmpfile"

	while grep ',' <"$tmpfile" > /dev/null; do
		sed -i 's|/\([^/]*\)/,|/\1/\1|' "$tmpfile"
	done

	echo "$SEDCOMMANDS" | sed -f - "$tmpfile" | colrm 1 1
}

reverse() {
	SEDCOMMANDS="$(echo "$CODES" | sed 's/^\([^:]*\):\(.*\)$/s|\/\2\/|\/\1\/|g/')"
	tmpfile="$(mktemp)"
	trap 'rm "$tmpfile"' 0 1 15
	sed 's|^|/|' > "$tmpfile"

	echo "$SEDCOMMANDS" | sed -i -f - "$tmpfile" | colrm 1 1

	rev <"$tmpfile" | sponge "$tmpfile"
	while grep -P '([^/]*)/\1/' "$tmpfile" > /dev/null; do
		sed -i 's|\([^/]*\)/\1/|,/\1/|' "$tmpfile"
	done
	rev <"$tmpfile"
}

OLDFILES="$(mktemp)"
NEWFILES="$(mktemp)"
trap  'rm "$OLDFILES" "$NEWFILES"' 0 1 15
find -maxdepth 1 -type f | xargs -d"\n" -I{} stat -c "%W %n" "{}" | filter > "$NEWFILES"
find -maxdepth 1 -type f > "$OLDFILES"

paste -d@ "$OLDFILES" "$NEWFILES" | while IFS="@" read -r f1 f2; do
	[ "$f1" = "$f2" ] && continue
	! [ -d "$(dirname "$f2")" ] && mkdir -p "$(dirname "$f2")"
	mv "$f1" "$f2"
done
