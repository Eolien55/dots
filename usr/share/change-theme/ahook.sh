#!/bin/sh
# SET WALLPAPER #
if [ "$_choice" != "current" ] ; then
	# So, here we pick a wallpaper randomly, from the corresponding directory
	find "$XDG_DATA_HOME/wall/$choice/" -type f | shuf -n1 | xargs -I{} ln -sf "{}" "$XDG_CONFIG_HOME/wall.png"

	xwallpaper --zoom "$XDG_CONFIG_HOME/wall.png"
fi

# SET XRESOURCES #
xrdb "$XDG_CONFIG_HOME/X11/xresources"

# Reload all programs
killall dwmblocks ; setsid -f dwmblocks > /dev/null 2>&1
killall dunst ; setsid -f dunst > /dev/null 2>&1
killall xsettingsd ; setsid -f xsettingsd > /dev/null 2>&1
killall -USR1 st
dwmc load-xresources

autorm
